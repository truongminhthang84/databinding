//
//  File.swift
//  
//
//  Created by Kien Nguyen on 11/25/19.
//

import Foundation

public extension LiveData {
    static func combine<T1, T2, E>(_ source1: LiveData<T1>,
                                   _ source2: LiveData<T2>,
                                   resultSelector: @escaping ((T1, T2) -> E)) -> LiveData<E> {
        let combined = CombinedLiveData(resultSelector(source1.value, source2.value))
        
        combined.addSource(source1) { e in
            combined.value = resultSelector(e, source2.value)
        }
        
        combined.addSource(source2) { e in
            combined.value = resultSelector(source1.value, e)
        }
        
        return combined
    }
    
    static func combine<T1, T2, T3, E>(_ source1: LiveData<T1>,
                                       _ source2: LiveData<T2>,
                                       _ source3: LiveData<T3>,
                                       resultSelector: @escaping ((T1, T2, T3) -> E)) -> LiveData<E> {
        let combined = CombinedLiveData(resultSelector(source1.value, source2.value, source3.value))
        
        combined.addSource(source1) { e in
            combined.value = resultSelector(e,
                                        source2.value,
                                        source3.value)
        }
        
        combined.addSource(source2) { e in
            combined.value = resultSelector(source1.value,
                                        e,
                                        source3.value)
        }
        
        combined.addSource(source3) { e in
            combined.value = resultSelector(source1.value,
                                        source2.value,
                                        e)
        }
        
        return combined
    }
}

class CombinedLiveData<Element>: MutableLiveData<Element> {
    func addSource<T>(_ source: LiveData<T>, selector: @escaping (T) -> Void) {
        source.addObserver(observer: selector)
    }
}
