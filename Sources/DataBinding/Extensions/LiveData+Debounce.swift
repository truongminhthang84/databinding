//
//  LiveData+Debounce.swift
//  
//
//  Created by Kien Nguyen on 11/29/19.
//

import Foundation

public extension LiveData {
    /// Create a LiveData that element is debounced within a duration
    /// - parameter:    duration   Debounce duration in milliseconds
    func debounce(_ duration: Int) -> LiveData<Element> {
        return DebouncedLiveData(source: self, duration: Double(duration) / 1000.0)
    }
}

class DebouncedLiveData<Element>: CombinedLiveData<Element> {
    
    private var timer: Timer?
    
    init(source: LiveData<Element>, duration: TimeInterval) {
        super.init(source.value)
        
        addSource(source) { e in
            self.handleElement(e, duration: duration)
        }
    }
    
    private func handleElement(_ e: Element, duration: TimeInterval) {
        if let timer = self.timer, timer.isValid {
            timer.invalidate()
        }
        
        if #available(iOS 10.0, *) {
            timer = Timer.scheduledTimer(
                withTimeInterval: duration, repeats: false, block: { [weak self] _ in
                    self?.value = e
            })
        } else {
            timer = Timer.scheduledTimer(timeInterval: duration,
                                         target: self,
                                         selector: #selector(self.updateValue(timer:)),
                                         userInfo: e,
                                         repeats: false)
        }
    }
    
    @objc private func updateValue(timer: Timer) {
        guard let element = timer.userInfo as? Element else {
            return
        }
        
        self.value = element
    }
}
