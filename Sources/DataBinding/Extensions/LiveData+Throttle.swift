//
//  LiveData+Throttle.swift
//  
//
//  Created by Kien Nguyen on 11/29/19.
//

import Foundation

public extension LiveData {
    /// Create a LiveData that element is throttled within a duration
    /// - parameter:    duration   Throttle duration in milliseconds
    func throttle(_ duration: Int) -> LiveData<Element> {
        return ThrottledLiveData(source: self, duration: Double(duration) / 1000.0)
    }
}

class ThrottledLiveData<Element>: CombinedLiveData<Element> {
    
    private var timer: Timer?
    private var isAvailable: Bool = true
    
    init(source: LiveData<Element>, duration: TimeInterval) {
        super.init(source.value)
        
        addSource(source) { e in
            self.handleElement(e, duration: duration)
        }
    }
    
    private func handleElement(_ e: Element, duration: TimeInterval) {
        if !isAvailable {
            return
        }
        
        value = e
        isAvailable = false
        
        if let timer = self.timer, timer.isValid {
            timer.invalidate()
        }
        
        if #available(iOS 10.0, *) {
            timer = Timer.scheduledTimer(
                withTimeInterval: duration, repeats: false, block: { [weak self] _ in
                    self?.isAvailable = true
            })
        } else {
            timer = Timer.scheduledTimer(timeInterval: duration,
                                         target: self,
                                         selector: #selector(updateAvailable),
                                         userInfo: nil,
                                         repeats: false)
        }
    }
    
    @objc private func updateAvailable() {
        isAvailable = true
    }
}
