//
//  File.swift
//  
//
//  Created by ThangTM-PC on 11/21/19.
//
//
import UIKit

extension UITextField: BindindTwoWay {
    
    public var observingValue: String? {
        get {
            return self.text
        }
        set {
            self.text = newValue
        }
    }
    public var defaultValue: String? {
        return ""
    }
    
    public typealias BindingType = String?
    
}

